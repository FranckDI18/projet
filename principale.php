<?php
session_start();
require_once('connexion.php');
?>
    <head>
        <title>MAJ TROMBI</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="principale.css" media="screen" type="text/css" />
        <script type="text/javascript" src="fonctions.js"></script>
    </head>
    <body>
        <div class ="content">

<!-- Tester si l'utilisateur est connecté -->

        <?php
        if (isset($_GET['deconnexion'])) {
            if ($_GET['deconnexion'] == true) {
                session_unset();
                header("location:login.php");
            }
        } else if ($_SESSION['username'] !== "") {
            $user = $_SESSION['username'];

            // Réinitialisation du poste
            if (isset($_POST['modification']) && $_POST['modification'] == '0') unset($_POST['service']);

// Afficher un message de bienvenue

            echo "<br>Bonjour $user ! Vous êtes connecté(e) !";
        }
        ?> 
        <div class = "TableauModif">
        <a href='principale.php?deconnexion=true' style="text-decoration:none"><input type="button" value="DECONNEXION"/></a>
        <h2>MISE A JOUR TROMBINOSCOPE</h2>

<!-- Début du formulaire -->

            <form method="post" enctype="multipart/form-data" name="ModifTrombi" id="ModifTrombi" action="principale.php">
                <?php

//Remplir le tableau de modif avec les infos de la base

                $bdd = new PDO('mysql:host=localhost;dbname=setintest;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                $reponse_societe = $bdd->query("SELECT numste, nomste FROM socpf ORDER BY numste");
                echo '<label for="societe" > Société : </label>';
                $societe_selection = "";
                if (isset($_POST['societe']))
                    $societe_selection = $_POST['societe'];
                echo '<select name="societe" id ="societe" onChange="this.parentNode.submit()"><option value="" selected>Toutes les sociétés</option>';
                while ($donnees = $reponse_societe->fetch()) {
                    echo '<option value="' . $donnees['numste'] . '"';
                    if (isset($_POST['societe']) && $_POST['societe'] == $donnees['numste']) echo 'selected="selected"';
                    echo ">" . $donnees['nomste'] . "</option>'";
                }
                echo '</select>';
                ?>
                <br /><br />
                <?php
                $where = "where 1=1 ";
                $join = " ";
                if ($societe_selection != "") {
                    $join .= ' LEFT JOIN socpf so on so.NUMSTE = rd.pvtrd ';
                    $where .= ' and rd.pvtrd = ' . $societe_selection;
                }
                $select = "SELECT redard, nomrd, pvtrd, codpos
                    FROM rdtcopf as rd  
                    " . $join . $where . "
                    ORDER BY rd.nomrd";
                $reponse_redac = $bdd->query($select) or die(print_r($bdd->errorInfo())); 

// Selectionner le rédacteur

                echo '<label for="redac" > Rédacteur : </label>';
                $redacteur_selection = "";
                if (isset($_POST['redac']))
                    $redacteur_selection = $_POST['redac'];
                echo '<select name="redac" id ="redac" onChange="VerifAndSubmit()"><option value="" selected>Sélectionner rédacteur</option>';
                while ($donnees = $reponse_redac->fetch()) {
                    echo '<option value="' . $donnees['pvtrd'] . $donnees['redard'] . '"';
                    if (isset($redacteur_selection) && $redacteur_selection == $donnees['pvtrd'] . $donnees['redard']) echo 'selected="selected"';
                    echo ">" . $donnees['redard'] . ' - ' . $donnees['nomrd'] . "</option>'";
                }
                echo '</select>';
                ?><br /><div id="Profil"><?php
                                        $photo = "";
                                        $poste = "";
                                        $FirstSelect = "";
                                        $nom = "";
                                        $NomImage = "";
                                        $CodeRedac = "";
                                        $CodeRedac = substr($redacteur_selection, 2);
                                        if (isset($_FILES['photo']) && $_FILES['photo']['tmp_name'] != '') {
                                            $NomImage = file_get_contents($_FILES['photo']['tmp_name']);
                                        }

// Envoyer la modification de la photo dans la base


                                        if ($NomImage != "" && $CodeRedac != "" && $_POST['modification'] == '1') {
                                            $req = $bdd->prepare("UPDATE images SET img_blob =:NewPhoto WHERE redard='$CodeRedac'");
                                            $req->execute(array(':NewPhoto' => $NomImage));
                                        }

// Afficher la photo du rédacteur sélectionné et le libéllé de son poste

                                        if ($redacteur_selection != "") {
                                            $bdd = new PDO('mysql:host=localhost;dbname=setintest;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                                            $reponse = $bdd->query("
                    SELECT DISTINCT if(nomrd ='',nomre ,nomrd) as nom,  img_blob as photo, libepv as PtDeVte, rd.codpos as poste
                    FROM rdtcopf as rd
                    left join reppf as re on re.reprcl = rd.redard
                    left join images as img on img.redard = rd.redard AND img.pvtrd = rd.pvtrd 
                    left join pvtpf as pv on pv.lieupv = rd.lieupv
                    WHERE concat(rd.pvtrd,rd.redard)='" . $redacteur_selection . "'") or die(print_r($bdd->errorInfo()));
                                            while ($donnees = $reponse->fetch()) {
                                                $photo = $donnees['photo'];
                                                $poste = $donnees['poste'];
                                                $nom = $donnees['nom'];
                                                echo '<img src="data:image/jpg;base64,' . base64_encode($photo) . '" height="185" width="140" class="imgprof">';
                                            }
                                            $FirstSelect = $poste;
                                        }
                                        ?>
                </div><br />
                <?php
                if (isset($_POST['service']) && $_POST['service'] != "") {
                    $poste = $_POST['service'];
                }
                $bdd = new PDO('mysql:host=localhost;dbname=setintest;charset=utf8', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION));
                $reponse_service = $bdd->query("
                    SELECT codpos, libpos 
                    FROM postpf
                    ORDER BY libpos") or die(print_r($bdd->errorInfo()));

// Afficher la liste des postes pour réaffectation

                echo '<label for="service" > Affecter à un nouveau service : </label>';
                echo '<select name="service" id ="service" onChange="NouvPoste()"><option value="" selected>Sélectionner un poste</option>';
                $LibPoste = $donnees['libpos'];
                while ($donnees = $reponse_service->fetch()) {
                    echo '<option value="' . $donnees['codpos'] . '"';
                    if ($poste == $donnees['codpos']) echo 'selected="selected"';
                    echo ">" . $donnees['libpos'] . "</option>'";
                }
                echo '</select>'; ?>

<!-- Détecter si un service est selectionné manuellement -->

                <script>
                function NouvPoste()
                {
                    document.getElementById('modification').value='1';
                }
        
// Mettre en place une fonction de preview pour la nouvelle photo

                function previewFile(fichier) {
                    var preview = document.querySelector('img');
                    if (fichier!=0){
                        alert(fichier);
                        var file    = fichier;
                    }
                    else{
                        var file    = document.querySelector('input[type=file]').files[0];
                    }
                    var reader  = new FileReader();
                    reader.onloadend = function () {
                    preview.src = reader.result;
                    }
                    if (file) {
                    reader.readAsDataURL(file);
                    document.getElementById('modification').value='1';
                    } else {
                    preview.src = "";
                    }
                }
                </script>

<!-- Importer nouvelle photo -->

                <br /><br />
                Modifier/ajouter photo :  
                <input type="file" name = "photo" id="photo" onchange="previewFile(0)"><br>
                <br /><?php
                        $poste = "";
                        if (isset($_POST['service'])) {
                            $poste = $_POST['service'];
                        } ?>
<input type="hidden" id="modification" name="modification" value="0"/>

<!-- Bouton de réinitialisation -->  

<input type="button" value="REINITIALISER" onclick=resetFields()><br/>
<script>
function resetFields()
{
document.getElementById('societe').value="";
document.getElementById('redac').value="";
document.getElementById('service').value="";
document.getElementById("ModifTrombi").submit();  
}
</script>

<!-- Afficher les messages de confirmation -->

                <script>
                    function test_modif()
                    {
                        var poste = "";
                        var LibPoste = "";
                        LibPoste = document.getElementById("service").options[document.getElementById('service').selectedIndex].text;
                        var CodeRedac = "";
                        var NomImage = "";
                        var photo= "";
                        var Nom = "";
                        var message = "";   
                        var poste = document.getElementById("service").value; 
                        var FirstSelect = '<?php echo $FirstSelect; ?>';
                        var CodeRedac = '<?php echo $CodeRedac; ?>';
                        var NomImage = document.getElementById("photo").value;
                        var Nom = '<?php echo $nom; ?>'; 
                        if (document.getElementById("service").value == "") {poste == "";}
                        if ('<?php echo $CodeRedac; ?>'== "") {CodeRedac == "";}
                        
                        if (poste != "" && poste != FirstSelect && FirstSelect != "" && NomImage == "" && CodeRedac != "" )
                        {
                            var message = "Vous allez modifier le POSTE de "+Nom+" en le remplaçant par "+LibPoste.toUpperCase()+", vous confirmez ?" ;
                        }
                        else if (FirstSelect == poste && NomImage != "" && CodeRedac != "" )
                        {
                            var message = "Vous allez modifier la PHOTO de "+Nom+" dans la base, vous confirmez ?" ;
                        }
                        else if (poste != "" && FirstSelect != "" && poste != FirstSelect && NomImage != "" && CodeRedac != "" )
                        {
                            var message = "Vous allez modifier le POSTE de "+Nom+" en le remplaçant par "+LibPoste.toUpperCase()+"\nVous allez modifier la PHOTO de "+Nom+" dans la base, vous confirmez ?" ;
                        }
                        else
                        {
                            var message= "Vous n'avez selectionné aucune modification, abandonner ?" ;
                        }
                        return confirm(message);
                    }
                </script>
                <input type="submit" name="bouton" value="VALIDER" onClick="return test_modif()";>
                <?php 
                
// Envoyer le poste modifié dans la base

                if ($poste != "" && $poste != $FirstSelect && $FirstSelect != "" && $CodeRedac != "" && $_POST['modification'] == '1') {
                    $req = $bdd->prepare("UPDATE rdtcopf SET codpos =:codpos WHERE redard='$CodeRedac'");
                    $req->execute(array(':codpos' => $poste));
                }
                ?> 
            </form>
            </div>
        </div>
    </body>
</html>
