<?php
/*
* Author : Ali Aboussebaba
* Email : bewebdeveloper@gmail.com
* Website : http://www.bewebdeveloper.com
* Subject : Autocomplete using PHP/MySQL and jQuery
*/

// PDO connect *********
function connect() {
    return new PDO('mysql:host=localhost;dbname=setintest', 'root', '', array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
}

$pdo = connect();
$db = mysqli_connect ("localhost", "root", "", "setintest");
$keyword = mysqli_real_escape_string($db,htmlspecialchars('%'.$_POST['keyword'].'%'));
$sql = "SELECT * 
FROM documents 
WHERE doc_motcle1 LIKE (:keyword) or doc_motcle2 LIKE (:keyword) or doc_motcle3 LIKE (:keyword) 
or doc_motcle4 LIKE (:keyword) or doc_motcle5 LIKE (:keyword) or doc_descr LIKE (:keyword)
ORDER BY doc_nom ASC LIMIT 0, 10";
$query = $pdo->prepare($sql);
$query->bindParam(':keyword', $keyword, PDO::PARAM_STR);
$query->execute();
$list = $query->fetchAll();
foreach ($list as $rs) {
	// put in bold the written text
	$doc_nom = str_replace($_POST['keyword'], '<b>'.$_POST['keyword'].'</b>', $rs['doc_nom']);
	// add new option
	$rs['doc_nom']=mysqli_real_escape_string($db,htmlspecialchars($rs['doc_nom']));
    echo '<li onclick="set_item(\''.$rs['doc_nom'].'\')">'.$doc_nom.'</li>';
}
?>