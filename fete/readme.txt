Fete du jour (sans base de donn�es)-----------------------------------
Url     : http://codes-sources.commentcamarche.net/source/28563-fete-du-jour-sans-base-de-donneesAuteur  : ded38frDate    : 22/08/2013
Licence :
=========

Ce document intitul� � Fete du jour (sans base de donn�es) � issu de CommentCaMarche
(codes-sources.commentcamarche.net) est mis � disposition sous les termes de
la licence Creative Commons. Vous pouvez copier, modifier des copies de cette
source, dans les conditions fix�es par la licence, tant que cette note
appara�t clairement.

Description :
=============

Le script fete.php affiche la f&ecirc;te du jour.
<br />La particularit&eacute;
 de ce script, compar&eacute; &agrave; ceux que j'ai d&eacute;ja pu trouver sur 
le Web, est qu'il n'utilise pas de base de donn&eacute;es. Il peut donc s'int&ea
cute;grer tr&egrave;s facilement sur un site.
<br /><a name='conclusion'></a><h
2> Conclusion : </h2>
<br />Pour plus d'informations, visitez mon site:
<br /
>  <a href='http://www.jerome-desmoulins.new.fr' target='_blank'>http://www.jero
me-desmoulins.new.fr</a>
<br />
<br />Une liste de diffusion est disponible su
r le site, vous pouvez vous inscrire pour &ecirc;tre inform&eacute; des mises &a
grave; jour du script
