<?php
  ///////////////////////////////////////////////////////////////////////////////
  // Fichier: fete.php                                                         //
  // Version: 1.00 - Derni�re modification: Samedi 1er Janvier 2005            //
  //                  http://www.jerome-desmoulins.new.fr                      //
  // Auteur:  J�r�me DESMOULINS (http://www.jerome-desmoulins.new.fr)          //
  // Description:                                                              //
  //   Ce script affiche, la f�te correspondante � aujourd'hui.                //
  //   Pour l'utiliser il suffit de placer include("fete.php"); dans un script //
  //   Attention: le fichier fete.txt doit se trouver dans le repertoire       //
  ///////////////////////////////////////////////////////////////////////////////

  // R�cup�ration de la date actuelle
  $jour=date("d");
  $mois=date("m");
  // Recherche de cette date dans le fichier de donn�es
  $fp=fopen("fete/fete.txt","r");
  while ( ! feof($fp) )
  {
    $ligne=fgets($fp,255);
    // On extrait le pr�nom f�t�
    $pos=strpos($ligne,';');
    $prenom=substr($ligne,0,$pos);
    $ligne=substr($ligne,$pos+1,strlen($ligne)-$pos);
    // Le jour de cette f�te
    $pos=strpos($ligne,';');
    $jourtrouve=substr($ligne,0,$pos);
    // Le mois de cette f�te
    $moistrouve=substr($ligne,$pos+1,strlen($ligne)-$pos-2);
    // Si on la trouve, on affiche la f�te du jour
    if (($jour==$jourtrouve) && ($mois==$moistrouve))
    {
      echo $prenom.'.';
    }
  }
  fclose($fp);

?>
