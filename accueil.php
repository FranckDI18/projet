<!DOCTYPE html>
<head>
        <title>ACCUEIL</title>
        <meta charset="utf-8">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
        <link rel="stylesheet" href="accueil.css" media="screen" type="text/css" />
        <script type="text/javascript" src="fonctions.js"></script>
    </head>
<?php 

$profil="test";
require_once('debug.php');
require_once('menuGauche.php');
require_once('connexion.php');
require_once('qui_visite_mon_site.php');
$NomComplet=", vous n'êtes pas connecté "."(<a href='loginintranet.php'>connexion</a>)";
if (isset($_SESSION['username'])) {
$query = $bdd->prepare('SELECT nom_utilisateur, NomComplet FROM utilisateur WHERE nom_utilisateur = :id');
$query->execute(['id' => $_SESSION['username']]);
$Reponse = $query->fetch(PDO::FETCH_ASSOC);
$NomComplet = $Reponse['NomComplet']; }
?>
<div id="Page_Principale">
    <h1>
        <?php 
        date_default_timezone_set('Europe/Paris');
        setlocale(LC_TIME, 'fr_FR');
        $date = strftime('%d/%m/%Y',mktime(date("H"), date("i"), date("s"), date("n"), date("j"), date("Y")));
        $heure = strftime('%H:%M',mktime(date("H"), date("i"), date("s"), date("n"), date("j"), date("Y")));
        echo '<br/>'.'Bienvenue '.$NomComplet.','.'<br/>';
        echo 'Nous sommes le '.$date.', il est '.$heure.' et nous fêtons les ';
        include('fete/fete.php');
?>  

    </h1>

    <body>
        <div id=Infos>
            <div id=encadre1>
                <ul><b>Informations générales :</b>
                    <p></p><li>Nous ouvrons une nouvelle agence aux sables d'Olonne.</li>
                    <li>Nous avons acquis la société Bigorre Soudure.</li>
                </ul>
            </div>
            <div id=encadre2>
                <ul><b>Infos CE :</b>
                    <p></p><li>Les chèques cadeaux pour la fête des pères sont disponibles.</li>
                    <li>Les entrées pour CERZA sont arrivées.</li>
                </ul>
            </div>
            <div id=encadre3>
                <ul><b>Informations diverses :</b>
                    <p></p><li>Aujourd'hui nous fêtons l'anniversaire de :</li>
                    - Emmanuel Braun <br/>
                    - Angélique Blondel
                </ul>
            </div>
            <div id=encadre4>
                <ul><b>Ephéméride :</b>
                    <p></p><li>Citation du jour : "Donne un poisson à un homme, il mangera un jour, apprends lui à pêcher, il mangera toute sa vie".</li>
                    <li>Proverbe du jour: Un "tiens" vaut mieux que deux "tu l'auras".</li>
                </ul>
            </div>
        </div>
    </body>
</div>
<footer class="footer">
    
</footer>