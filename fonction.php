<?php

require_once('connexion.php');


/* GetNomRedacteur retourne le nom correspondant à un code rédacteur et une société.
Si on n'indique uniquement une société elle retourne tous les rédacteurs rattachés */

function GetNomRedacteur ($Societe=null, $CodeRedac=null, $sql)
{   
    $tableau=Array();
    $i=0;
    $where = " 1=1 ";
    if ($Societe != '') $where .= " and soc.numste = '" . $Societe . "'";
    if ($CodeRedac != '') $where .= " and rd.redard = '" . $CodeRedac . "'";
    if ($CodeRedac = '') $where = " and 1=1 ";
    $query = mysqli_query($sql, "SELECT nomrd as nom
    FROM rdtcopf as rd
    LEFT JOIN socpf as soc on rd.pvtrd = soc.numste
    WHERE " . $where) or die(mysqli_error($sql));
    while ($rep = mysqli_fetch_assoc($query)){
    $tableau[$i]=$rep['nom'];
    $i++;
    }
    return $tableau;
}
// var_dump( GetNomRedacteur('01','', $sql));


/*  GetInfosRedacteur retourne les noms, points de vente, et mails correspondant à un code rédacteur et une société.
Si on n'indique uniquement une société, elle retourne tous les rédacteurs rattachés */

function GetInfosRedacteur ($Societe=null, $CodeRedac=null, $sql)
{   
    $where = " 1=1 ";
    if ($Societe != '') $where .= " and soc.numste = '" . $Societe . "'";
    if ($CodeRedac != '') $where .= " and rd.redard = '" . $CodeRedac . "'";    
    if ($CodeRedac = '') $where = " and 1=1 ";
    $query = mysqli_query($sql, "SELECT nomrd as nom, pvtrd as pv, emard as mail
    FROM rdtcopf as rd
    LEFT JOIN socpf as soc on rd.pvtrd = soc.numste
    WHERE " . $where) or die(mysqli_error($sql));
    while ($rep = mysqli_fetch_assoc($query)){
    echo $rep['nom'];
    echo $rep['pv'];
    echo $rep['mail'];
} 
}
// var_dump (GetInfosRedacteur('01','AA', $sql)); 



/*  GetPhotoRedacteur retourne la photo correspondant à un code rédacteur et une société. */

function GetPhotoRedacteur ($Societe=null, $CodeRedac=null, $sql)
{
    $where = " 1=1 ";
    if ($Societe != '') $where .= " and soc.numste = '" . $Societe . "'";
    if ($CodeRedac != '') $where .= " and rd.redard = '" . $CodeRedac . "'";
    if ($CodeRedac = '') $where = " and 1=1 ";
    $query = mysqli_query($sql, "SELECT nomrd as nom, img_blob as photo
    FROM rdtcopf as rd
    LEFT JOIN images as img on img.redard = rd.redard
    LEFT JOIN socpf as soc on rd.pvtrd = soc.numste
    WHERE " . $where) or die(mysqli_error($sql));
    while ($rep = mysqli_fetch_assoc($query)){
    $photo = "";
    $photo = $rep['photo'];
    echo '<img src="data:image/jpg;base64,' . base64_encode($photo) . '" height="300" width="230" class="imgprof">';}
}
//echo GetPhotoRedacteur('01','AA', $sql);


/*  GetPosteRedacteur retourne le poste correspondant à un code rédacteur et une société.*/

function GetPosteRedacteur ($Societe=null, $CodeRedac=null, $sql)
{
    $where = " 1=1 ";
    if ($Societe != '') $where .= " and soc.numste = '" . $Societe . "'";
    if ($CodeRedac != '') $where .= " and rd.redard = '" . $CodeRedac . "'";
    if ($CodeRedac = '') $where = " and 1=1 ";
    $query = mysqli_query($sql, "SELECT nomrd as nom, codpos as poste
    FROM rdtcopf as rd
    LEFT JOIN socpf as soc on rd.pvtrd = soc.numste
    WHERE " . $where) or die(mysqli_error($sql));
    while ($rep = mysqli_fetch_assoc($query)){
    echo $rep['poste'];}
}
// echo GetPosteRedacteur('01','AA', $sql);


/* GetPoste retourne la liste des postes */

function GetPoste ($sql)
{
    $where = " 1=1 ";
    $query = mysqli_query($sql, "SELECT codpos as poste
    FROM rdtcopf as rd
    WHERE " . $where) or die(mysqli_error($sql));
    while ($rep = mysqli_fetch_assoc($query)){
    echo $rep['poste'];}
}
// echo GetPoste($sql);


/*  GetPosteParNom retourne le poste correspondant à un nom et une société.*/

function GetPosteParNom ($Societe=null, $NomRd=null, $sql)
{
    $where = " 1=1 ";
    if ($Societe != '') $where .= " and soc.numste = '" . $Societe . "'";
    if ($NomRd != '') $where .= " and rd.nomrd = '" . $NomRd . "'";
    if ($NomRd = '') $where = " and 1=1 ";
    $query = mysqli_query($sql, "SELECT nomrd as nom, codpos as poste
    FROM rdtcopf as rd
    LEFT JOIN socpf as soc on rd.pvtrd = soc.numste
    WHERE " . $where) or die(mysqli_error($sql));
    while ($rep = mysqli_fetch_assoc($query)){
    echo $rep['poste'];}
}
// echo GetPosteParNom('01','Annie ARNOU', $sql); 


/*  GetNomParPoste retourne la liste des noms rattachés à un poste.*/

function GetNomParPoste ($Poste=null, $sql)
{
    $where = " 1=1 ";
    if ($Poste != '') $where .= " and rd.codpos = '" . $Poste . "'";
    $query = mysqli_query($sql, "SELECT nomrd as nom
    FROM rdtcopf as rd
    WHERE " . $where) or die(mysqli_error($sql));
    while ($rep = mysqli_fetch_assoc($query)){
    echo $rep['nom'];}
}
// echo GetNomParPoste('ADM DIR', $sql);


 
/*  GetDocParNom retourne une documentation en l'appelant pas son nom. */

function GetDocParNom ($doc_nom=null, $sql)
{
    $query = mysqli_query($sql, "SELECT doc_blob, doc_nom
    FROM documents
    WHERE doc_nom = '$doc_nom'") or die(mysqli_error($sql));
    $rep = mysqli_fetch_assoc($query);
    header('Content-Description: File Transfer');
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Content-Type: application/pdf') ;
    header( 'Content-Disposition: inline; filename="'.$doc_nom.'.pdf"');
    header('Pragma: public');
    ob_clean();
    flush();
    echo $rep['doc_blob'];
    exit;
}
echo GetDocParNom("Reliquats", $sql);




?>


