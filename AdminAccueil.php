<?php
session_start();
require_once('connexion.php');
?>
<head>
    <title>ACCUEIL_ADMIN</title>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <link rel="stylesheet" href="accueil.css" media="screen" type="text/css" />
    <script type="text/javascript" src="fonctions.js"></script>
</head>
<div id="PagePrincipale"></div>
    <div id="Infos">
        <form method="POST" name="Encadre" id ="Encadre" action="RemplirEncadre.php">
            <label for="Encadre1">Texte de l'encadré N° 1</label>
            <textarea style="width:400px; height:200px;" name="Encadre1" maxlength="400"></textarea>
            <label for="Encadre2">Texte de l'encadré N° 2</label>
            <textarea style="width:400px; height:200px;" name="Encadre2" maxlength="400"></textarea>
            <label for="Encadre3">Texte de l'encadré N° 3</label>
            <textarea style="width:400px; height:200px;" name="Encadre3" maxlength="400"></textarea>
            <label for="Encadre4">Texte de l'encadré N° 4</label>
            <textarea style="width:400px; height:200px;" name="Encadre4" maxlength="400"></textarea>
        </form>
    </div>
</div>