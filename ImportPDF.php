<?php
session_start();
require_once('connexion.php');

$DocNom ="";
$DocSize ="";
$DocDescr="";
$DocBlob="";
$DocType="";
$DocMotCle1="";
$DocMotCle2="";
$DocMotCle3="";
$DocMotCle4="";
$DocMotCle5="";
$pdo="";

if (isset($_POST) && isset($_FILES['PDF']) && $_FILES['PDF']['name'] != '') 
{
    $DocNom = ($_FILES['PDF']['name']);
    $DocSize = ($_FILES['PDF']['size']);
    $DocDescr = $_POST['DocDescr'];
    $DocBlob = file_get_contents($_FILES['PDF']['tmp_name']);
    $DocType = 'PDF';
    $DocMotCle1 = $_POST['MotCle1'];
    $DocMotCle2 = $_POST['MotCle2'];
    $DocMotCle3 = $_POST['MotCle3'];
    $DocMotCle4 = $_POST['MotCle4'];
    $DocMotCle5 = $_POST['MotCle5'];

    // echo $DocNom.' '.$DocDescr.' '.$DocType.' '.$DocMotCle1.' '.$DocMotCle2.' '.$DocMotCle3.' '.$DocMotCle4.' '.$DocMotCle5;


$req = $bdd->prepare('INSERT INTO documents (doc_nom, doc_descr, doc_blob, doc_type, doc_motcle1, doc_motcle2, doc_motcle3, doc_motcle4, doc_motcle5,doc_size)
VALUES (:DocNom, :DocDescr, :DocBlob, :DocType, :DocMotCle1, :DocMotCle2, :DocMotCle3, :DocMotCle4, :DocMotCle5, :DocSize)');
$req->execute([
'DocNom' => $DocNom,
'DocSize' => $DocSize,
'DocDescr' => $DocDescr,
'DocBlob' => $DocBlob,
'DocType' => $DocType,
'DocMotCle1' => $DocMotCle1,
'DocMotCle2' => $DocMotCle2,
'DocMotCle3' => $DocMotCle3,
'DocMotCle4' => $DocMotCle4,
'DocMotCle5' => $DocMotCle5
]) or die(print_r($bdd->errorInfo()));

}   

header('Location: UploadPDF.php');