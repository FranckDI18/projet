<html>
    <head>
        <title>MENU PAO</title>
        <meta charset="utf-8">
        <!-- importer le fichier de style -->
        <link rel="stylesheet" href="principale.css" media="screen" type="text/css" />
    </head>
    <body>
        <div id="content">
            <!-- tester si l'utilisateur est connecté -->
            <?php
            session_start();
            if(isset($_GET['deconnexion']))
            {
                if($_GET['deconnexion']==true)
                {
                    session_unset();
                    header("location:login.php");
                }
            }
            else if($_SESSION['username'] !== ""){
                $user = $_SESSION['username'];
                // afficher un message
                echo "<br>Bonjour $user ! Vous êtes connecté(e) !";
            }
            ?>
            <div class = "TableauModif">
            <a href='principale.php?deconnexion=true' style="text-decoration:none"><input type="button" value="DECONNEXION"/></a>
                <form method="post" name="ModifTrombi" action="principale.php">
                    <?php
                    $bdd = new PDO('mysql:host=localhost;dbname=setintest;charset=utf8', 'root','', array(PDO::ATTR_ERRMODE => PDO ::ERRMODE_EXCEPTION));
                    $reponse_societe = $bdd->query("SELECT numste, nomste FROM socpf ORDER BY numste");
                    echo '<label for="societe" > Société : </label>';
                    $societe_selection="";
                     if (isset($_POST['societe']))
                    $societe_selection = $_POST['societe'];
                    /* echo $societe_selection; */
                    echo '<select name="societe" id ="societe" onChange="this.parentNode.submit()"><option value="" selected>Toutes les sociétés</option>';
                    while ($donnees = $reponse_societe->fetch())
                    {
                        echo '<option value="'.$donnees['numste'].'"';
                        if (isset($_POST['societe']) && $_POST['societe'] == $donnees['numste']) echo 'selected="selected"';
                        echo ">".$donnees['nomste']."</option>'";
                    }
                    echo '</select>';
                    ?>
                    <br /><br />
                    <?php
                    $where = "where 1=1 ";
                    $join = " ";
                    if ($societe_selection != "") 
                    { 
                        $join .= ' LEFT JOIN socpf so on so.NUMSTE = rd.pvtrd ';
                        $where .= ' and rd.pvtrd = '.$societe_selection;
                    }

                    $bdd = new PDO('mysql:host=localhost;dbname=setintest;charset=utf8', 'root','', array(PDO::ATTR_ERRMODE => PDO ::ERRMODE_EXCEPTION));
                    $select = "SELECT redard, nomrd, pvtrd, codpos
                        FROM rdtcopf as rd  
                        ".$join.$where."
                        ORDER BY rd.nomrd";
                    
                    $reponse_redac = $bdd->query($select) or die(print_r($bdd->errorInfo()));
                    echo '<label for="redac" > Rédacteur : </label>';
                    $redacteur_selection = "";
                    if (isset($_POST['redac']))
                    $redacteur_selection = $_POST['redac']; 
                    /* echo $redacteur_selection; */
                    echo '<select name="redac" id ="redac" onChange="this.parentNode.submit()"><option value="" selected>Sélectionner rédacteur</option>';
                    while ($donnees = $reponse_redac->fetch())
                    {
                        echo '<option value="'.$donnees['pvtrd'].$donnees['redard'].'"';
                        if (isset($redacteur_selection ) && $redacteur_selection  == $donnees['pvtrd'].$donnees['redard']) echo 'selected="selected"';
                        echo ">".$donnees['redard'].' - '.$donnees['nomrd']."</option>'";
                    }
                    echo '</select>';
                    
                    ?><br /><div id="Profil"><?php
                   $photo="";
                   $poste="";
                   $nom="";
                    if ($redacteur_selection !="")
                    {
                            $bdd = new PDO('mysql:host=localhost;dbname=setintest;charset=utf8', 'root','', array(PDO::ATTR_ERRMODE => PDO ::ERRMODE_EXCEPTION));
                            $reponse = $bdd->query("
                             SELECT DISTINCT if(nomrd ='',nomre ,nomrd) as nom,  img_blob as photo, libepv as PtDeVte, rd.codpos as poste
                            FROM rdtcopf as rd
                        left join reppf as re on re.reprcl = rd.redard
                        left join images as img on img.redard = rd.redard AND img.pvtrd = rd.pvtrd 
                        left join pvtpf as pv on pv.lieupv = rd.lieupv
                        WHERE concat(rd.pvtrd,rd.redard)='".$redacteur_selection."'") or die(print_r($bdd->errorInfo()));
                        while ($donnees = $reponse->fetch())
                        {
                            $photo=$donnees['photo'];
                            $poste=$donnees['poste'];
                            $nom=$donnees['nom'];
                            echo '<img src="data:image/jpg;base64,'.base64_encode($photo).'" height="185" width="140" class="imgprof">';
                        }
                        
                    }
                    ?>
                    </div><br />
                    <?php
                    $bdd = new PDO('mysql:host=localhost;dbname=setintest;charset=utf8', 'root','', array(PDO::ATTR_ERRMODE => PDO ::ERRMODE_EXCEPTION));
                    $reponse_service = $bdd->query("
                        SELECT codpos, libpos 
                        FROM postpf
                        ORDER BY libpos") or die(print_r($bdd->errorInfo()));
                    echo '<label for="service" > Affecter à un nouveau service : </label>';
                    echo '<select name="service" id ="service">';
                    while ($donnees = $reponse_service->fetch())
                    {
                        echo '<option value="'.$donnees['codpos'].'"';
                        if ($poste == $donnees['codpos']) echo 'selected="selected"';
                        echo ">".$donnees['libpos']."</option>'";
                    }
                    echo '</select>';?>
                    <script>
                    function previewFile() {
                      var preview = document.querySelector('img');
                      var file    = document.querySelector('input[type=file]').files[0];
                      var reader  = new FileReader();
                      reader.onloadend = function () {
                        preview.src = reader.result;
                      }
                      if (file) {
                        reader.readAsDataURL(file);
                      } else {
                        preview.src = "";
                      }
                    }
                    </script>
                    <br /><br />
                    Modifier/ajouter photo :  
                    <input type="file" name = "photo" onchange="previewFile()"><br>
                    <br /><br /><?php
                    $CodeRedac ="";
                    $CodePoste = $poste;
                    $NomImage = "";
                    $CodeRedac = substr($redacteur_selection, 2);
                    if (isset ($_POST['photo'])) {$NomImage = $_POST['photo'];}
                    if (isset ($_POST['service'])) {$CodePoste = $_POST['service'];}
                    ?>
                    <input type="submit" name="bouton" value="VALIDER"/>
                    <?php 
                    if (isset($_POST['bouton']))
                    {   
                        if ($CodePoste != $poste AND $CodeRedac != "")
                        {
                            echo"<script language=\"javascript\">";
                            echo"if(confirm('Vous allez modifier le SERVICE de $nom dans la base de données, vous confirmez ?'))alert();";
                            echo"</script>";

                        }
                        if ($NomImage !="" AND $CodeRedac != "")
                        {
                            $confirm = "";
                            echo"<script language=\"javascript\">";
                            echo"confirm('Vous allez modifier la PHOTO de $nom dans la base de données, vous confirmez ?')";
                            echo"</script>";
                        }
                        

                       
                        /*
                         ?><br /><?php
                        echo $CodeRedac;
                        ?><br /><?php
                        echo $CodePoste; 
                        ?><br /><?php
                        echo $NomImage;
                        */

                        
                    }
                    ?>
                </form>
            </div>
        </div>
    </body>
</html>
