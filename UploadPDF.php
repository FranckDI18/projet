<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<html class="no-js"> 
<?php
session_start();
require_once('connexion.php');

?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>IMPORT DOCUMENTS</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="principale.css">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
</head>
<h1>


</h1>
<body>
    <div class="content">
        <?php
        if (isset($_GET['deconnexion'])) {
            if ($_GET['deconnexion'] == true) {
                session_unset();
                header("location:login.php");
            }
        } else if ($_SESSION['username'] !== "") {
            $user = $_SESSION['username'];
            echo "<br>Bonjour $user ! Vous êtes connecté(e) !";
        }
        ?>
        <a href='principale.php?deconnexion=true' style="text-decoration:none"><input type="button" value="DECONNEXION"/></a>
        <br/>
        <h2>IMPORT DOCUMENTS</h2>
        <form method="POST" action="ImportPDF.php" enctype="multipart/form-data" name="ImportPDF" id="ImportPDF">
            <br/><input type="file" name="PDF" id="PDF" accept="application/pdf"><br/>
            <input type="hidden" name="NomDoc">
            <br/><label for="DocDescr" >Description du document (200 caractères maximum):</label>
            <br/><input type="text" name="DocDescr" id="DocDescr" placeholder="Entrez une description" maxlength="200" style="width:100%"><br/>
            <br/><label for="MotCle1">Entrez un mot clé aidant à la recherche du document :</label>
            <br/><input type="text" name="MotCle1" id="MotCle1" placeholder="Mot clé 1" style="width:100%" maxlength="40"><br/>
            <br/><label for="MotCle2">Entrez un mot clé aidant à la recherche du document :</label>
            <br/><input type="text" name="MotCle2" id="MotCle2" placeholder="Mot clé 2" style="width:100%" maxlength="40"><br/>
            <br/><label for="MotCle3">Entrez un mot clé aidant à la recherche du document :</label>
            <br/><input type="text" name="MotCle3" id="MotCle3" placeholder="Mot clé 3" style="width:100%" maxlength="40"><br/>
            <br/><label for="MotCle4">Entrez un mot clé aidant à la recherche du document :</label>
            <br/><input type="text" name="MotCle4" id="MotCle4" placeholder="Mot clé 4" style="width:100%" maxlength="40"><br/>
            <br/><label for="MotCle5">Entrez un mot clé aidant à la recherche du document :</label>
            <br/><input type="text" name="MotCle5" id="MotCle5" placeholder="Mot clé 5" style="width:100%" maxlength="40"><br/>
            <br/><br/><input type="submit" id="BoutonPDF" value="VALIDER">
           
        </form>
    </div>
</body>
</html>
