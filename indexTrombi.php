<!DOCTYPE html>
<html>
    <head>
        <title>TROMBINOSCOPE</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="trombi.css">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    </head>
    <h1>
<!-- Affichage du menu fixe à gauche -->

<?php 
$profil="test";
require_once('debug.php');
require_once('menuGauche.php');
require_once('qui_visite_mon_site.php');
require_once('connexion.php');
?>

<!-- Affichage du titre fixe-->

        <div class="BlocHaut">
            <div class="Titre">
                <span><a href="http://localhost/tests/indexTrombi.php" style="text-decoration:none">TROMBINOSCOPE</a></span>
            </div>
    
<!-- Affichage du formulaire de recherche fixe-->    
    
            <div class = "FormulaireRecherche">
                <form method="post" name="Trombi" action="indexTrombi.php">
                    <input type="text" name="recherche" id="recherche" style="height:20px;" placeholder="Recherche à partir du nom, prénom, N° de téléphone, email et/ou sélection des options suivantes :" size="124"/>
                    <input type="submit" class ="BoutRech" value="RECHERCHE"/>
                    <br />
                    <?php
                    include('liste_societe.php');
                    include('liste_pdvte.php');
                    include('liste_service.php');
                    ?>
                    <a href="http://localhost/tests/indexTrombi.php" style="text-decoration:none">
                        <input type="button" class="Reinit" value="REINITIALISATION"/><br/><br/>
                    </a>
                </form>
            </div>
        </div>
    </h1>

<!-- Affichage du résultat de la requête -->

    <body>
    <div class = "MaPage">
        <div class="TableauReponses">
            <?php
            $where = " 1=1 ";
            $saisie = "";
            $pdvte = "";
            $societe = "";
            $service = "";
            if (isset($_POST['recherche']))
                $saisie = mysqli_real_escape_string($sql,htmlspecialchars($_POST['recherche']));
            if ($saisie != '')
                $where .= " and (rd.nomrd LIKE '%$saisie%' or rd.telrd LIKE '%$saisie%' or re.nomre LIKE '%$saisie%' or rd.emard LIKE '%$saisie%')";
            if (isset($_POST['pdvte']))
                $pdvte = $_POST['pdvte'];
            if ($pdvte != '')
                $where .= " and rd.lieupv = '" . $pdvte . "'";
            if (isset($_POST['societe']))
                $societe = $_POST['societe'];
            if ($societe != '')
                $where .= " and rd.pvtrd = '" . $societe . "'";
            if (isset($_POST['service']))
                $service = $_POST['service'];
            if ($service != '')
                $where .= " and rd.codpos = '" . $service . "'";
            $where .= "ORDER BY pv.numste,rd.lieupv, service";
            $affichage_societe = "";
            $affichage_ptdevte = "";
            $affichage_service = "";
            if (isset($saisie) or isset($pdvte) or isset($societe) or isset($service)) {
                $reponse = $bdd->query("
                    SELECT DISTINCT if(nomrd ='',nomre,nomrd) as nom, if(telrd = '',telre,telrd) as tel, emard as mail, img_blob as photo, libpos as service, libepv as PtDeVte, nomste as societe
                    FROM rdtcopf as rd
                    left join reppf as re on re.reprcl = rd.redard
                    left join images as img on img.redard = rd.redard AND img.pvtrd = rd.pvtrd
                    left join postpf as ser on ser.codpos = rd.codpos
                    left join pvtpf as pv on pv.lieupv = rd.lieupv
                    left join socpf as soc on rd.pvtrd = soc.numste
                    WHERE " . $where) or die(print_r($bdd->errorInfo()));
                while ($donnees = $reponse->fetch()) {

// Affichage barre de titre "société" 

                    if ($affichage_societe != $donnees['societe']) {
                        $affichage_societe = $donnees['societe'];
                        ?><div class ="Titre_societe"><?php
                        echo $affichage_societe;
                        ?></div><?php
                    }

// Affichage nom du point de vente

                    if ($affichage_ptdevte != $donnees['PtDeVte']) {
                        $affichage_ptdevte = $donnees['PtDeVte'];
                        ?><div class ="Titre_ptdevte"><?php
                        echo 'Site : '.$affichage_ptdevte;?><hr id="HR1"></div><?php
                        
                    }

// Affichage barre de titre "service"
                        
                    if ($affichage_service != $donnees['service']) {
                        $affichage_service = $donnees['service'];
                        ?><div class ="Titre_service"><?php
                        echo 'Service : '.$affichage_service; ?><hr id="HR2"></div><?php
                    }

// Affichage des profils

                            $photo = "";
                            $photo = $donnees['photo']; ?>
                <div class="Profil"><br/><br/>
                        <?php echo '<img src="data:image/jpg;base64,' . base64_encode($photo) . '" height="300" width="230" class="imgprof">';
                        ?><br/><br/><?php
                        echo $donnees['nom'];
                        ?><br/><?php
                        echo 'Tel : ' . $donnees['tel'];
                        ?><br/><?php
                        echo 'mail : ' . '<a href="mailto:' . $donnees['mail'] . '" style="text-decoration:none">' . $donnees['mail'] . '</a>';
                        ?><br/><?php
                        echo $donnees['service'];
                        ?><br/><?php
                        echo $donnees['PtDeVte'];
                        ?></div><?php
                        }
                        $reponse->closeCursor();
                                } ?>
                </div>
    </div>
        
    </body>
</html>

