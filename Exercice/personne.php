<?php

$personnes = [
    'id1' => [
        'nom' => 'DUPONT',
        'prenom' => 'Robert',
        'age' => '30'
    ],
    'id2' => [
        'nom' => 'Durand',
        'prenom' => 'Paul',
        'age' => '16'
    ],
    'id3' => [
        'nom' => 'Doe',
        'prenom' => 'John',
        'age' => '25'
    ]
];

 foreach ($personnes as $id) {

      if ($id['age'] >= 18) {
      echo $id['prenom'].' est majeur'."\n";}
}

?>