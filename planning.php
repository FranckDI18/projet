<!DOCTYPE html>
<html>
<?php 
$profil="test";
require_once('debug.php');
require_once('menuGauche.php');
require_once('qui_visite_mon_site.php');
?>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Panning</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="planning.css">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <script type="text/javascript" src="js/script.js"></script>
</head>
<body>
    <div id="animations">
        <ul><p>Planning des animations commerciales<br/></p>
            <li>Le 25 avril 2019 : Journée porte-ouverte à l'agence de Saint-lô.</li>
            <li>Le 5 mai 2019 : Journée démonstration avec le fournisseur Dewalt à Martot <a href=""> (INSCRIPTION) </a></li>
            <li>Le 12 juin 2019 : Formation Dorma sur une demie-journeé à l'agence de Dieppe</li>
            <li>Le 15 septembre 2019 : Journée phoning Spax à Martot</li>
            <li>Les 18 et 19 octobre 2019 : Présentation Bosch à Lisieux</li>
        </ul>
    </div>
</body>
</html>