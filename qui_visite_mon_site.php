<?php
date_default_timezone_set('Europe/Paris');
setlocale(LC_TIME, 'fr_FR');
$date = strftime('%d/%m/%Y');
$heure = strftime('%H:%M:%S');
if (!empty(['REMOTE_ADDR']))
{
    $adresse_ip   = $_SERVER['REMOTE_ADDR'];
}
else
{
    $adresse_ip   = $_SERVER['HTTP_X_FORWARDED_FOR'];
}
if(!empty($_SERVER['REQUEST_URI']))
{
    $adresse_page = $_SERVER['REQUEST_URI'];
}
else
{
    $adresse_page = $_SERVER['SCRIPT_NAME'];
}
$provenance = "inconnue";
if(isset($_SERVER['HTTP_REFERER'])&& !empty($_SERVER['HTTP_REFERER'])) $provenance = $_SERVER['HTTP_REFERER'];
$agent = $_SERVER['HTTP_USER_AGENT'];
$langue = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
if ((strpos($agent, 'Trident')) !== false) {
    $browser = "Internet Explorer";
} else if (preg_match('/Chrome[\/\s](\d+\.\d+)/', $agent)) {
    $browser = "Chrome";
} else if (preg_match('/Edge\/\d+/', $agent)) {
    $browser = "Edge";
} else if (preg_match('/Firefox[\/\s](\d+\.\d+)/', $agent)) {
    $browser = "Firefox";
} else if (preg_match('/OPR[\/\s](\d+\.\d+)/', $agent)) {
    $browser = "Opera";
} else if (preg_match('/Safari[\/\s](\d+\.\d+)/', $agent)) {
    $browser = "Safari";
} else {
    $browser = "Non reconnu : (" . $agent . ")";
}
$fp = @fopen("Journal_connexion.txt", "a+");
if ($fp) {
        fputs($fp, "Date: $date  Heure: $heure  IP: $adresse_ip  Page: $adresse_page  Provenance: $provenance  Navigateur: $browser  Langue(s): $langue\r\n");
    } else {
        echo "Erreur d'ouverture du fichier";
    }
fclose($fp);
