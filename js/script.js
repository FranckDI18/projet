
// autocomplet : this function will be executed every time we change the text
function autocomplet() {
	var min_length = 1;
	var keyword = $('#BarreRech').val();
	if (keyword.length >= min_length) {
	$.ajax({
		url: 'ajax_refresh.php',
		type: 'POST',
		data: {keyword:keyword},
		success:function(data){
			$('#Doc_list').show();
			$('#Doc_list').html(data);	
			$('#TitreDoc').hide();
			$('#doc').hide();
	}
	});
	} 
	else 
	{
		$('#Doc_list').hide();
		$("#RechDoc").submit();
		$("#BarreRech").focus();
		
	}
}	

// set_item : this function will be executed when we select an item
function set_item(item) {
	// change input value
	$('#BarreRech').val(item);
	// Submit form
	$("#RechDoc").submit();
	// hide proposition list
	$('#Doc_list').hide();
	

}