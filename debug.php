<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="debug.css" />
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <script src="js/main.js"></script>
</head>
<body>
</body>
<div id="hidebutton"><button id="hide" name="hide" onclick="Cacher()">Masquer debugger</button></div>
<div id="refresh"><button id="refresh" name="refresh" onclick="Refresh()">Actualiser</button></div>
<?php

if ($profil != "test")
{
    echo '<div id="debug" style="display:none">';
    echo '<script>document.getElementById("hidebutton").style.display="none"</script>';
    echo '<script>document.getElementById("refresh").style.display="none"</script>';
}
else
{
    
    echo '<div id="debug">';
    
}
?>
    <p>
        <div id="erreurs">
            <?php
            $file = file("C:\wamp64\logs/php_error.log");
            $monfichier = fopen('erreurs.txt', 'r+');
            ftruncate($monfichier,0);
            for ( $i = count($file) - 50 > 0 ? count($file) - 50 : count($file) ; $i < count($file) ; $i++ ) {
            fputs($monfichier, $file[$i]);
            }
            $lines = file('erreurs.txt');  
            for($i=count($lines);$i>0;$i--){
                echo (51-$i).' : '.' '.$lines[$i-1].'<br />';
            }
            ?>
        </div>
    </p>
</div>
</html>
