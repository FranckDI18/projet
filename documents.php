<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Documentation</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="doc.css">
    <link href="https://fonts.googleapis.com/css?family=Titillium+Web" rel="stylesheet">
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/script.js"></script>
</head>
<?php  
$profil="test";
require_once('debug.php');
require_once('menuGauche.php');
require_once('qui_visite_mon_site.php');
?>
    <h1>

<!-- Affichage du titre fixe-->

    <div id="BlocHaut">
            <div class="Titre">
                <span><a href="http://localhost/tests/documents.php" style="text-decoration:none">DOCUMENTATION</a></span>
            </div>
    
<!-- Affichage du formulaire de recherche fixe-->    
    
    
            <div class = "FormulaireRecherche">
                <?php require_once('connexion.php')?> 
                <form method="POST" name="RechDoc" id="RechDoc" action="documents.php">
                    <div class="input_container">
                        <input type="text" id="BarreRech" name="BarreRech" style="height:20px;" placeholder=" Recherche par mots clés ou nom ex: commande spéciale" size="100" autocomplete="off" onkeyup="autocomplet()">
                        <input type="submit" id="BoutonRecherche" value="RECHERCHER"/><br/>
                        <ul id="Doc_list"></ul>
                    </div>
                </form>
            </div>

    <!-- Affichage des titres de la liste de documents -->

        <div id="TitreDoc">
            <div id="nomDoc">NOM</div>
            <div id="descDoc">DESCRIPTION</div>
            <div id="motCleDoc">MOTS CLES</div>
        </div>
    </div>
    </h1>
    <body>
    <form method="post" name "VisuPDF" action="download.php">
    <div id="doc">
        <?php 
        $where = " 1=1 ";
        $saisie = "";
        $size = "";
        $type = "";
        $nom = "";
        $pdf = "";
        $id_doc = "";
        $db = mysqli_connect ("localhost", "root", "", "setintest");
        if (isset($_POST['BarreRech'])) $saisie = mysqli_real_escape_string($db,htmlspecialchars($_POST['BarreRech']));
        if (isset($saisie)) { $where .= " and (doc_nom LIKE '%$saisie%' or doc_descr LIKE '%$saisie%' or doc_motcle1 LIKE '%$saisie%' or doc_motcle2 LIKE '%$saisie%' or doc_motcle3 LIKE '%$saisie%' or doc_motcle4 LIKE '%$saisie%' or doc_motcle5 LIKE '%$saisie%')";
        $where .= "ORDER BY doc_nom";
        $reponse = $bdd->query("
        SELECT DISTINCT doc_blob, doc_nom, doc_descr, doc_motcle1, doc_motcle2, doc_motcle3, doc_motcle4, doc_motcle5, doc_type, doc_size, ID_doc
        FROM documents
        WHERE " . $where) or die(print_r($bdd->errorInfo()));
        while ($donnees = $reponse->fetch()) {
            $size=$donnees['doc_size'];
            $type=$donnees['doc_type'];
            $nom=$donnees['doc_nom'];
            $descr=$donnees['doc_descr'];
            $motcle1=$donnees['doc_motcle1'];
            $motcle2=$donnees['doc_motcle2'];
            $motcle3=$donnees['doc_motcle3'];
            $motcle4=$donnees['doc_motcle4'];
            $motcle5=$donnees['doc_motcle5'];
            $pdf =$donnees['doc_blob'];
            $id_doc = $donnees['ID_doc'];
            echo '<a href="download.php?id='.$id_doc.'" id="LienDoc" style="text-decoration: none" target="_blank"><div id="RepNom">'.$nom.'</div></a>' ?>
            <div id="RepDesc"><?php echo $descr;?>
            </div><div id="RepMotCle"><?php  
            if ($motcle1!="") echo $motcle1;
            if ($motcle2!="") echo', '.$motcle2;
            if ($motcle3!="") echo', '.$motcle3;
            if ($motcle4!="") echo', '.$motcle4;
            if ($motcle5!="") echo', '.$motcle5;?><br><br>
            </div><?php  
        }
        } 
        ?>
        </form>
        </div>
        <body onload="document.forms['RechDoc'].elements['BarreRech'].focus()">
    </body>
</html>

